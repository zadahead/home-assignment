import styles from './Inner.module.css';

interface IProps {
    children: React.ReactElement
}

export default function({ children }: IProps) {
    return (
        <div className={styles.container}>
            {children}
        </div>
    )
}