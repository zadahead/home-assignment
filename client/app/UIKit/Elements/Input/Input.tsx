import { ReactElement, useState } from 'react';
import styles from './Input.module.css';
import { IconEye, IconEyeOpen } from '../Icon/Icon';

interface IProps {
    focus?: boolean;
    error?: boolean;
    p?: string;
    onChange: (val: string) => void;
    value: string;
    empty?: () => void;
    i?: ReactElement;
    type?: InputType
}

type InputType = 'text' | 'password';

export const Input  = (props: IProps) => {
    const [dispPass, setDispPass] = useState(false);
    const { focus, error, type: inputType, i: icon, p, value, onChange } =props;

    const type: InputType = inputType ==='password' && !dispPass ? 'password' : 'text';

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.value);
    }

    const renderEye = () => {
        if(inputType !== 'password') {
            return <></>
        }

        return (
            <div onClick={() => setDispPass(!dispPass)}>
                {dispPass ? <IconEyeOpen /> : <IconEye />}
            </div>
        )
    }
    return (
        <div className={styles.container} data-error={error}>
            {icon}
            <input 
                value={value} 
                onChange={handleChange} 
                placeholder={p} 
                type={type} 
            />
            {renderEye()}
        </div>
    )
}