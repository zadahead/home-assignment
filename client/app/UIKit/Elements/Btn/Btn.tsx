import { ReactElement } from 'react';
import styles from './Btn.module.css';

type Elem = JSX.Element | JSX.Element[] | string;
interface IProps {
    disabled?: boolean;
    onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
    children?: Elem;
    isLoading?: boolean;
    variant?: TVariant;
    i?: ReactElement;
}

type TVariant = 'invert' | 'regular'

export const Btn = (props: IProps) => {
    const {
        disabled, onClick, isLoading, children, variant, i: icon
    } = props;

    const isDisabled = disabled;

    const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        if (isLoading) { return }

        onClick(e);
    }
    return (
        <button 
            className={styles.container} 
            onClick={handleClick} 
            disabled={isDisabled}
            data-variant={variant}
        >
            {icon}
            {children}
        </button>
    )
}
