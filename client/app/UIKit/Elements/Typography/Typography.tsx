import styles from './Typography.module.css';
import './variants.css';

interface IProps {
    children: string;
    variant?: TVariant;
}

type TVariant = 'gray' | 'primary' | 'white';


export const Headline = ({ children, variant }: IProps) => {
    return <h1 className={styles.headline} data-variant={variant}>{ children }</h1>
}
export const Text = ({ children, variant }: IProps) => {
    return <p className={styles.text} data-variant={variant}>{ children }</p>
}