import LogoSvg from '~/assets/logo.svg';
import LogoDarkSvg from '~/assets/logoDark.svg';
import IllustrationSvg from '~/assets/illustration.svg';
import EmailSvg from '~/assets/email.svg';
import LockSvg from '~/assets/lock.svg';
import EyeSvg from '~/assets/eye.svg';
import EyeOpenSvg from '~/assets/eyeOpen.svg';
import FacebookSvg from '~/assets/fb.svg';
import GoogleSvg from '~/assets/google.svg';

export const IconLogo = () => <img src={LogoSvg} />
export const IconLogoDark = () => <img src={LogoDarkSvg} />
export const IconIllustration = () => <img src={IllustrationSvg} />
export const IconEmail = () => <img src={EmailSvg} />
export const IconLock = () => <img src={LockSvg} />
export const IconEye = () => <img src={EyeSvg} />
export const IconEyeOpen = () => <img src={EyeOpenSvg} />
export const IconFacebook = () => <img src={FacebookSvg} />
export const IconGoogle = () => <img src={GoogleSvg} />