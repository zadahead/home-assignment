import type { MetaFunction } from "@remix-run/node";

import styles from './login.module.css';
import Center from "~/UIKit/Layouts/Center/Center";
import Inner from "~/UIKit/Layouts/Inner/Inner";
import { IconEmail, IconFacebook, IconGoogle, IconIllustration, IconLock, IconLogo, IconLogoDark } from "~/UIKit/Elements/Icon/Icon";
import { Headline, Text } from "~/UIKit/Elements/Typography/Typography";
import { Input } from "~/UIKit/Elements/Input/Input";
import { useInput } from "~/hooks/useInput";
import { Link } from "@remix-run/react";
import { Btn } from "~/UIKit/Elements/Btn/Btn";
import { useValidator } from "~/hooks/useValidator";
import axios from "axios";

export const meta: MetaFunction = () => {
    return [
        { title: "Life Medic - Login" },
        { name: "description", content: "Login page" },
    ];
};


export default function () {
    const Email = useInput("", "Email", "empty, email");
    const Password = useInput("", "Password", "empty");

    const validation = useValidator(Email, Password);

    const isDisabled = !(Email.value && Password.value);

    const handleLogin = () => {
        if(!validation()) { return }

        axios.post('http://127.0.0.1:5000/login', {
            email: Email.value,
            password: Password.value
        }).then((resp:any) => {
            alert('login success!');
        }).catch((err: any) => {
            alert(err.response.data.error);
        })
    }

    return (
        <Inner>
            <Center>
                <section className={styles.container} data-media="login">
                    <div>
                        <div className={styles.logo}>
                            <IconLogo />
                        </div>
                        <Center>
                            <div className={styles.greetings}>
                                <IconIllustration />
                                <Headline>Welcome aboard my friend</Headline>
                                <Text>just a couple of clicks and we start</Text>
                            </div>
                        </Center>
                    </div>
                    <main className={styles.main}>
                        <Center>
                            <div className={styles.loginWrap}>
                                <div data-media="logo-dark">
                                    <Center>
                                        <IconLogoDark />
                                    </Center>
                                </div>
                                <Center>
                                    <Headline variant="primary">Log in</Headline>
                                </Center>
                                <div className={styles.loginGroup}>
                                    <Input i={<IconEmail />} {...Email} />
                                    <Input i={<IconLock />} {...Password} type="password" />
                                    <div className={styles.loginSide}>
                                        <Link to='/'>Forgot Password?</Link>
                                    </div>
                                </div>

                                <div className={styles.loginGroup}>
                                    <Btn onClick={handleLogin} disabled={isDisabled}>Log in</Btn>
                                    <div className={styles.loginSep}>
                                        <Text variant="gray">Or</Text>
                                    </div>
                                    <div className={styles.loginLine}>
                                        <Btn onClick={handleLogin} i={<IconFacebook />} variant="invert">Facebook</Btn>
                                        <Btn onClick={handleLogin} i={<IconGoogle />} variant="invert">Google</Btn>
                                    </div>

                                </div>

                                <div className={styles.loginGroup}>
                                    <Center>
                                        <Text variant="gray">Have no account yet?</Text>
                                    </Center>
                                    <Btn onClick={handleLogin} variant="invert">Register</Btn>
                                </div>
                            </div>
                        </Center>
                    </main>
                </section>
            </Center>
        </Inner>
    )
}