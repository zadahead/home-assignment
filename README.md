# Client Installation 

1. Install
```
npm install
```
2. Run
```
npm start
```

# Native App Installation
1. Install
```
npm install
```
2. Run
```
npm start
```

# Server Installation
1. Python 3.11
2. Create python virtual environment ``
```
--create env 
python3.11 -m venv myenv

--activate env
source myenv/bin/activate

--deactivate env
deactivate
```
3. Install requirements from requirements.txt
```
pip install -r requirements.txt
```

## Install MongoDB locally with docker and run
```
docker run -d -p 27017:27017 --name m1 mongo
```

## Run
Run local server using the following command 
```
flask run
flask run --debug
```
