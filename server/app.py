import os
from dotenv import load_dotenv
from flask import request, Flask
from flask_cors import CORS
from mongo import userLogin, getAllUsers

load_dotenv()

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": os.getenv('CLIENT_ORIGIN')}})

@app.get("/users")
def get_users():
    return getAllUsers()

@app.post("/login")
def login():
    user = userLogin(request.json['email'], request.json['password'])
    if(user == -1):
        return { 'error': 'User credentials not valid'}, 401
    
    return user, 201