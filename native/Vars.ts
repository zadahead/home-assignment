export const vars = {
    primary: '#3949AB',
    primaryBackground: "#5769D4",
    primaryBackground100: "#E6E9FA",
    primaryBackground500: "#3949AB",
    primaryBackground500Hover: "#27368F",

    white: "#fff",
    gray: "#7B7B7B",
    border: "#D0D0D0",

    
    gap: 8,
    gapS: 10,
    gapL: 12,
}