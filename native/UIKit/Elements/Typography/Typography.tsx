import { StyleSheet, Text as TextNative } from 'react-native';
import { vars } from '../../../Vars';

interface IProps {
    children: string;
    variant?: TVariant;
}

type TVariant = 'gray' | 'primary' | 'white';


export const Headline = ({ children, variant }: IProps) => {
    return <TextNative style={[styles.headline, styles[variant]]}>{ children }</TextNative>
}
export const Text = ({ children, variant }: IProps) => {
    return <TextNative style={[styles.text, styles[variant]]}>{ children }</TextNative>
}

const styles = StyleSheet.create({
    headline: {
        fontSize: 40
    },
    text: {
        fontSize: 20
    },
    white: {
        color: vars.white
    },
    gray: {
        color: vars.gray
    },
    primary: {
        color: vars.primary
    }
})