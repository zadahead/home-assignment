import { ReactElement } from 'react';
import { GestureResponderEvent, Pressable, StyleSheet, View } from 'react-native';
import { vars } from '../../../Vars';
import { Headline, Text } from '../Typography/Typography';

interface IProps {
    disabled?: boolean;
    onClick: (e: GestureResponderEvent) => void;
    children?: string;
    isLoading?: boolean;
    variant?: TVariant;
    i?: ReactElement;
}

type TVariant = 'invert' | 'regular'

export const Btn = (props: IProps) => {
    const {
        disabled, onClick, isLoading, children, variant, i: icon
    } = props;

    const isDisabled = disabled;

    const handleClick = (e: GestureResponderEvent) => {
        if (isLoading) { return }

        onClick(e);
    }
    return (
        <View style={styles.wrap}>
            <Pressable
                style={({pressed}) => [
                    styles.container, 
                    styles[variant], 
                    disabled && styles.disabled,
                    pressed && (variant === 'invert' ? styles.pressedInvert : styles.pressed)
                ]}
                onPress={handleClick}
                disabled={isDisabled}
            >
                {icon}
                <Text variant={variant === 'invert' ? 'primary' : 'white'}>{children}</Text>
            </Pressable>
        </View>
    )
}


const styles = StyleSheet.create({
    wrap: {
        height: 50,
    },
    container: {
        backgroundColor: vars.primaryBackground500,
        color: vars.white,
        flex: 1,
        height: 50,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        gap: vars.gap
    },
    pressed: {
        backgroundColor: vars.primaryBackground500Hover,
    },
    invert: {
        backgroundColor: vars.white,
        borderColor: vars.primaryBackground500,
        borderWidth: 1
    },
    pressedInvert: {
        borderColor: vars.primaryBackground500Hover,
        opacity: 0.8
    },
    disabled: {
        opacity: 0.3
    }
})