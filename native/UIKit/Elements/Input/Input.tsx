import { ReactElement, useState } from 'react';
import IconEyeOpen from '../../Svg/EyeOpen';
import IconEye from '../../Svg/Eye';
import { Pressable, StyleSheet, TextInput, View } from 'react-native';
import { vars } from '../../../Vars';

interface IProps {
    error?: boolean;
    p?: string;
    onChange: (val: string) => void;
    value: string;
    empty?: () => void;
    i?: ReactElement;
    type?: InputType
}

type InputType = 'text' | 'password';

export const Input = (props: IProps) => {
    const [dispPass, setDispPass] = useState(false);
    const [isFocused, setIsFocused] = useState(false);

    const { error, type: inputType, i: icon, p, value, onChange } = props;

    const type: InputType = inputType === 'password' && !dispPass ? 'password' : 'text';

    const renderEye = () => {
        if (inputType !== 'password') {
            return <></>
        }

        return (
            <Pressable onPress={() => setDispPass(!dispPass)}>
                {dispPass ? <IconEyeOpen /> : <IconEye />}
            </Pressable>
        )
    }
    return (
        <View style={styles.wrap}>
            <View style={[
                styles.container, 
                isFocused && styles.containerFocused,
                error && styles.error
                ]} >
                {icon}
                <TextInput
                    style={styles.input}
                    value={value}
                    onChangeText={onChange}
                    placeholder={p}
                    secureTextEntry={type === 'password'}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                />
                {renderEye()}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrap: {
        height: 50,
    },

    container: {
        borderColor: vars.border,
        borderWidth: 1,
        height: 50,
        borderRadius: 8,
        paddingHorizontal: 15,
        paddingVertical: 10,
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        gap: 10
    },

    containerFocused: {
        shadowColor: '#5769D4',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3, 
        shadowRadius: 3,
        elevation: 3, 
        borderColor: vars.primaryBackground500,
    },

    error: {
        borderColor: 'red'
    },

    input: {
        flex: 1
    }
})