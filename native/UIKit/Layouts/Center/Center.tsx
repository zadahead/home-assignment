import { StyleSheet, View } from 'react-native';

interface IProps {
    children: React.ReactElement
}

export default function({ children }: IProps) {
    return (
        <View style={styles.container}>
            {children}
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center"
    }
})