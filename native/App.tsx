import axios from 'axios';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import { useInput } from './hooks/useInput';
import { useValidator } from './hooks/useValidator';
import Center from './UIKit/Layouts/Center/Center';
import IconLogoDark from './UIKit/Svg/LogoDark';
import IconEmail from './UIKit/Svg/Email';
import IconLock from './UIKit/Svg/Lock';
import { Input } from './UIKit/Elements/Input/Input';
import { Headline, Text } from './UIKit/Elements/Typography/Typography';
import { Btn } from './UIKit/Elements/Btn/Btn';
import IconFacebook from './UIKit/Svg/Facebook';
import IconGoogle from './UIKit/Svg/Google';
import { vars } from './Vars';

export default function App() {
  const Email = useInput("", "Email", "empty, email");
  const Password = useInput("", "Password", "empty");

  const validation = useValidator(Email, Password);

  const isDisabled = !(Email.value && Password.value);

  const handleLogin = () => {
    if (!validation()) { return }

    axios.post('http://127.0.0.1:5000/login', {
      email: Email.value,
      password: Password.value
    }).then((resp) => {
      alert('login success!');
    }).catch((err) => {
      const message = err?.response?.data?.error || err?.message || "Error";
      alert(message);
    })
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollView} >
        <View style={styles.margin}>
          <Center>
            <IconLogoDark />
          </Center>
        </View>
        <View style={styles.margin}>
          <Center>
            <Headline variant="primary">Log in</Headline>
          </Center>
        </View>
        <View style={styles.loginGroup}>
          <Input i={<IconEmail />} {...Email} />
          <Input i={<IconLock />} {...Password} type="password" />
          <View style={styles.loginSide}>
            <Text variant="primary">Forgot Password?</Text>
          </View>
        </View>

        <View style={styles.loginGroup}>
          <Btn onClick={handleLogin} disabled={isDisabled}>Log in</Btn>
          <View style={styles.loginSep}>
            <Text variant="gray">Or</Text>
          </View>
          <View>
            <View style={styles.loginLine}>
              <View style={styles.loginLineChild}>
                <Btn onClick={handleLogin} i={<IconFacebook />} variant="invert">Facebook</Btn>
              </View>
              <View style={styles.loginLineChild}>
                <Btn onClick={handleLogin} i={<IconGoogle />} variant="invert">Google</Btn>
              </View>
            </View>
          </View>

        </View>

        <View style={styles.loginGroup}>
          <Center>
            <Text variant="gray">Have no account yet?</Text>
          </Center>
          <Btn onClick={handleLogin} variant="invert">Register</Btn>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollView: {
    alignItems: 'stretch',
    justifyContent: 'center',
    flex: 1
  },
  margin: {
    marginBottom: 20
  },
  loginGroup: {
    marginBottom: 50,
    marginHorizontal: 30,
    gap: vars.gap
  },
  loginSide: {
    alignSelf: "flex-end"
  },
  loginSep: {
    alignSelf: "center"
  },
  loginLine: {
    flexDirection: "row",
    gap: vars.gapL
  },
  loginLineChild: {
    flex: 1,
    gap: 5
  }
});
