import { useState } from "react";
import { IUseInputResponse } from "./useInput.types";

export const useValidator = (...inputs: IUseInputResponse[]) => {
    const [isActive, setIsActive] = useState(false);

    const validate = () => {
        inputs.forEach(input => {
            setValidation(input);
        });
    }

    if (isActive) {
        validate();
    }

    const isValid = () => {
        setIsActive(true);
        validate();
        return !inputs.find(i => i.validation.error);
    }

    return isValid;
}

const setValidation = (input: IUseInputResponse) => {

    const { prefix = '' } = input.validation;
    const list = prefix.split(',');

    for (let i = 0; i < list.length; i++) {
        const p = list[i].trim();
        const error = check(p, getVal(input) || '');
        input.error = false;
        if (error) {
            input.validation.error = error as string
            input.error = true;
            break
        }
    }
}

const getVal = (input: IUseInputResponse) => {
    switch(input.validation.type) {
        case "checkbox": return input.checked;
        case "radio": return input.selected;
        case "file": return input?.data;
        default: return input.value;
    }
}

const check = (prefix: string, value: string | boolean = '') => {
    switch (prefix) {
        case "empty": {
            if (!value && typeof(value) !== 'boolean') {
                return "Cannot be empty";
            }
            break;
        }
        case "email": {
            var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (!(value as string).match(validRegex)) {
                return "not valid email"
            }
            break;
        }
       
    }
}