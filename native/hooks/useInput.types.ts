export interface IUseInputResponse {
    value: string;
    onChange: (val: string) => void;
    p?: string;
    validation: { 
        type: string
        error: string,
        prefix?: string
    };
    empty: () => void;
    checked?: boolean;
    selected?:boolean;
    data?: string;
    error: boolean;
}